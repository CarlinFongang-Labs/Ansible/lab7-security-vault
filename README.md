# Ansible | Ansible vault

_______

**Prénom** : Carlin

**Nom** : FONGANG

**Email** : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Afin de protéger les données sensibles, nous allons utiliser Ansible Vault pour sécuriser les variables d'environnement.

## Objectifs

Dans ce lab, nous allons :

- Créer une machine cliente et une machine hôte pour Ansible.

- Créer un répertoire files qui contiendra un dossier .secrets, et dans ce dossier, on aura un fichier nommé credentials.vault.

- Déplacer le contenu de la clé publique devops-aCD.pem dans le nouveau fichier credentials.vault.

- Encrypter le fichier à l'aide de la commande ansible-vault encrypt.

- Modifier le playbook deploy.yml afin qu'il charge le fichier vaulté en tant que vars_files.

- Générer une paire de clés (ssh-keygen -t rsa) en laissant tous les paramètres par défaut.

- Copiez le contenu de la clé publique (id_rsa.pub) dans le fichier /home/$USER/.ssh/authorized_keys de l'instance cible.

- Modifier le fichier d'inventaire afin de rajouter cette variable à tous les hôtes (all): ansible_ssh_common_args='-o StrictHostKeyChecking=no'.

- Lancer le playbook en rajoutant le paramètre qui vous permettra de fournir la clé vault.

## Prérequis
Disposer de deux machines avec Ubuntu déjà installées.

Dans notre cas, nous allons provisionner des instances EC2 s'exécutant sous Ubuntu via AWS, sur lesquelles nous effectuerons toutes nos configurations.

Documentation complémentaire :

[Documentation Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

## 1. Création des instances EC2
[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

## 2. Création du repertoire files/secrets/ et du fichier credentials.yml

````bash
mkdir files
mkdir files/.secrets
nano files/.secrets/credentials.yml
````
contenu du fichier credentials.pem

````bash
vault_ansible_password: mot_de_passe_admin
````

## 3. vaultage du fichier credentials.pem

````bash
ansible-vault encrypt file/.secrets/credentials.yml
````
une fois cette commande entrée, il faudra définir un mot de passe 

>![alt text](img/image.png)


On peut tester l'encryptage du mot de passe en l'affichant : 

>![alt text](img/image-1.png)
*Encryptage du contenu de la clé ssh*


## 4. Modification du fichier group_vars/prod.yml

````bash
nano group_vars/prod.yml
````

contenu du fichier prod.yml

````bash
ansible_user: ubuntu
ansible_password: "{{ vault_ansible_password }}"
````

## 5. Modification du fichier deploy.yml

````bash 
nano deploy.yml
````

mise à jour du contenu du fichier 

````bash
- name: "Installation d'Apache à l'aide de Docker"
  hosts: prod
  become: true
  vars_files:
    - files/.secrets/credentials.yml
````

## 6. Exécution du nouveau playbook

````bash
ansible-playbook -i hosts.yml --ask-vault-pass deploy.yml 
````

Lors de lancement du playbook, le mot de passe de deveroullage vault sera demandé afin de lire le fichier encrypté.

il est possible de retrouver l'emplacement du fichier contenant le mot de passe grace à la commande : 

````bash
grep -r "ansible_password" .
````


## 7. Gestion des clés

pour la génération d'un paire de clé : 
 
````bash
ssh-keygen -t rsa
````

>![alt text](img/image-2.png)
*Génération de clé ssh*

### 7.1. Copie de la clé publique sur la machine cible 

1. **Méthode manuelle**
````bash
cat .ssh/id_rsa.pub
````

Une fois le contenu copié en presse papier 

````bash
ssh -i devops-aCD.pem ubuntu@public_ip_client
nano .ssh/authorized_keys
````

Collez votre clé publique SSH (le contenu du fichier id_rsa.pub de votre machine locale) à la fin du fichier authorized_keys. Tout en s'assurez-vous qu'elle est sur une seule ligne et qu'il n'y a pas d'espaces ou de lignes supplémentaires ajoutées.

>![alt text](img/image-3.png)
*Ajout d'un nouvelle clé publique*

2. Méthode en ligne de commande

````bash
ssh-copy-id ubuntu@public_ip_client
````

3. Modification du ansible.cfg
nous allons commenter le chemin vers la clé devops-aCD.pem afin que la nouvelle clé générée soit utilisée

````bash
[defaults]
host_key_checking = False
remote_user = ubuntu
#private_key_file = files/.secrets/devops-aCD.pem
````

4. Exécution du playbook

````bash
ansible-playbook -i hosts.yml deploy.yml
````

>![alt text](img/image-4.png)

>![alt text](img/image-5.png)

L'application est mise en ligne